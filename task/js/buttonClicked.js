var btn_add = document.getElementById('add');
var btn_clear = document.getElementById('clear');

var status_changed = true;
var total_cnt = 0;
var cnt = 0;

//设置监听器

//在添加按钮失去焦点时
btn_add.onmouseleave = function (event) {
    status_changed = true;
}

//在按钮点击时触发
btn_add.onclick = create_tag;
btn_clear.onclick = clear_tag;


// TODO 可优化存储内容

//窗口新建时读取
window.onload = function(event){
    var tags = document.getElementById("tags"); 
    tags.innerHTML = localStorage.getItem("tags");
    textarr = localStorage.getItem("texts").split(",");
    Array.from(tags.children).forEach(tag => {
        set_movable(tag);
        tag.lastElementChild.value = textarr.shift();
    });
    
    total_cnt = tags.childElementCount - 1;
}

//窗口关闭时保存
window.onbeforeunload = function(event){
    var tags = document.getElementById("tags"); 
    localStorage.setItem("tags",tags.innerHTML);
    textarr = [];
    Array.from(tags.children).forEach(tag => {
        textarr.push(tag.lastElementChild.value);
    });
    localStorage.setItem("texts",textarr);
}

function create_tag() {
    if (status_changed) {
        status_changed = false;
        cnt = 0;
    }

    //创建一个新div
    var div = create_div();

    //创建标题
    var title = document.createElement("div");
    title.innerHTML = "便签" + total_cnt;
    var divattr = document.createAttribute("class");
    divattr.value = "tag_title_bar";
    title.setAttributeNode(divattr);

    divattr = document.createAttribute("id");
    divattr.value = "tag_title_bar" + total_cnt;
    title.setAttributeNode(divattr);

    

    div.appendChild(title);

    //创建内容
    var text = document.createElement("textArea");

    var divattr = document.createAttribute("class");
    divattr.value = "tag_content";
    text.setAttributeNode(divattr);

    divattr = document.createAttribute("id");
    divattr.value = "tag_content" + total_cnt;
    text.setAttributeNode(divattr);

    div.appendChild(text);

    var position = (function () {
        var x;
        var y;
        if (status_changed||cnt==0) {
            x = y = 0;
        } else {
            var tags = document.getElementById("tags");    
            
            width =parseInt(getComputedStyle(tags.children[0]).width);
            height =parseInt(getComputedStyle(tags.children[0]).height); 
            var x_max = parseInt(tags.clientWidth / width);
            x = parseInt(cnt % x_max) * width;
            y = parseInt(cnt / x_max) *height;
        }
        //留出按钮div的位置

        var o = document.getElementById("buttons");
        var h = o.clientHeight||o.offsetHeight;
        y+=h;
        return { x,y };
    })();

    div.style.left = position.x +'px';
    div.style.top = position.y+'px';




    //TODO 设置位置
    //加入网页中
    document.getElementById("tags").appendChild(div);
    set_movable(div);
    total_cnt++;
    cnt++;
    
}

//创建一个新窗口
function create_div(){
    var div = document.createElement("div");
    //为div创建属性class id 
    var divattr = document.createAttribute("class");


    divattr.value = "tag";
    div.setAttributeNode(divattr);

    divattr = document.createAttribute("id");
    divattr.value = "tag" + total_cnt;
    div.setAttributeNode(divattr);
    return div;
}


function clear_tag() {
    var tags = document.getElementById("tags")
    var child = tags.lastElementChild;
    while (child) {
        tags.removeChild(child);
        child = tags.lastElementChild;
    }
    total_cnt = 0;
}

function set_movable(tag) {
    var title = tag.children[0];

    //被拖动时
    title.onmousedown = function (event) {
        var event = event || window.event;
        var x = event.clientX - tag.offsetLeft; //当前鼠标点击处相对于tag所在位置x
        var y = event.clientY - tag.offsetTop; //当前鼠标点击处相对于title所在位置y
       
        document.onmousemove = function (event) {
            tag.style.left = event.clientX - x + "px";
            tag.style.top = event.clientY - y + "px";

            //鼠标当前位置
            if(window.getSelection()){
                window.getSelection().removeAllRanges();
            }else{
                document.selection.empty();
            } 
        }
    }

    //取消拖动后
    document.onmouseup = function () {
        document.onmousemove = null;
    }
}
