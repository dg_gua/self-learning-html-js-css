1. 关于拖动功能，参考[这个](scrolling_page.html)

1. 对于新增功能，参考[这个](https://jingyan.baidu.com/article/e9fb46e16b4d5f7521f766bf.html)

1. 多css文件，参考[https://blog.csdn.net/sunshine5050/article/details/78187486](https://blog.csdn.net/sunshine5050/article/details/78187486)

1. js脚本导入，参考[https://www.php.cn/js-tutorial-473933.html]

1. js新建div，参考[https://blog.csdn.net/qq_29145989/article/details/51034183]

1. 清空子元素，参考 [https://www.jianshu.com/p/f9b0bfd11851]